﻿using System;
using System.Linq;
using Imdb.Domain;
using System.Collections.Generic;

namespace Imdb
{
    class Program
    {
        static void Main(string[] args)
        {
            var movieService = new MovieService();
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("1. List Movies");
                Console.WriteLine("2. Add Movie");
                Console.WriteLine("3. Add Actor");
                Console.WriteLine("4. Add Producer");
                Console.WriteLine("5. Delete Movie");
                Console.WriteLine("6. Exit");
                Console.Write("Your option : ");
                var choice = int.Parse(Console.ReadLine());
                Console.WriteLine();
                if (choice == 6)
                    break;
                if (choice == 1)
                {
                    ListMovies(movieService);
                }
                if (choice == 2)
                {
                    try
                    {
                        int flag = 0;
                        var allActors = movieService.ReturnActors();
                        if (allActors.Count == 0)
                        {
                            Console.WriteLine("Please add some actors first!");
                            continue;
                        }
                        var allProducers = movieService.ReturnProducers();
                        if (allProducers.Count == 0)
                        {
                            Console.WriteLine("Please add a producer first");
                            continue;
                        }

                        Console.Write("Name: ");
                        var name = Console.ReadLine();
                        int strint;
                        if (int.TryParse(name, out strint))
                        {
                            Console.WriteLine("Not a valid name");
                            continue;
                        }

                        Console.Write("Year of release: ");
                        var yearOfRelease = int.Parse(Console.ReadLine());
                        if (yearOfRelease < 1888 || yearOfRelease > 2021)
                        {
                            Console.WriteLine("Enter a valid year!");
                            continue;
                        }
                        Console.Write("Plot: ");
                        var plot = Console.ReadLine();
                        Console.WriteLine();

                        Console.Write("Choose actor(s): ");
                        for (int i = 0; i < allActors.Count; i++)
                        {
                            Console.Write("{0}. {1} ", i + 1, allActors[i].Name);
                        }
                        Console.WriteLine();
                        var inputActor = Console.ReadLine();
                        var indexOfActors = inputActor.Split(" ");
                        var actors = new List<Actor>();
                        foreach (var i in indexOfActors)
                        {
                            int index = int.Parse(i);
                            if (index < 1 || index > allActors.Count)
                            {
                                Console.WriteLine("Enter valid actors");
                                flag = 1;
                                break;
                            }
                            actors.Add(allActors[index - 1]);
                        }
                        if (flag == 1)
                            continue;

                        Console.Write("Choose producer: ");
                        for (int i = 0; i < allProducers.Count; i++)
                        {
                            Console.Write("{0}. {1} ", i + 1, allProducers[i].Name);
                        }
                        Console.WriteLine();
                        var inputProducer = Console.ReadLine();
                        if (inputProducer.Length > 2)
                        {
                            Console.WriteLine("Enter only one producer");
                            continue;
                        }
                        var indexOfProducer = int.Parse(inputProducer);
                        if (indexOfProducer < 1 || indexOfProducer > allProducers.Count)
                        {
                            Console.WriteLine("Enter valid producer!");
                            continue;
                        }
                        var producer = allProducers[indexOfProducer - 1];

                        movieService.AddMovie(name, yearOfRelease, plot, actors, producer);
                        Console.WriteLine("Movie added!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        continue;
                    }
                }
                if (choice == 3)
                {
                    try
                    {
                        Console.Write("Name: ");
                        var name = Console.ReadLine();
                        Console.Write("DOB: ");
                        var dateOfBirth = DateTime.Parse(Console.ReadLine());
                        movieService.AddActor(name, dateOfBirth);
                        Console.WriteLine("Actor added!");
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Wrong format of date");
                        continue;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        continue;
                    }
                }
                if (choice == 4)
                {
                    try
                    {
                        Console.Write("Name: ");
                        var name = Console.ReadLine();
                        Console.Write("DOB: ");
                        var dateOfBirth = DateTime.Parse(Console.ReadLine());
                        movieService.AddProducer(name, dateOfBirth);
                        Console.WriteLine("Producer added!");
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Wrong format of date");
                        continue;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        continue;
                    }
                }
                if (choice == 5)
                {
                    ListMovies(movieService);
                    Console.WriteLine();
                    try
                    {
                        Console.Write("Enter movie name to delete: ");
                        movieService.DeleteMovie(Console.ReadLine());
                        Console.WriteLine("Movie deleted!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        continue;
                    }
                }
                if (choice < 1 || choice > 6)
                {
                    Console.WriteLine("Invalid choice");
                    continue;
                }
            }
        }
        public static void ListMovies(MovieService movieService)
        {
            var movies = movieService.GetAllMovies();
            if (movies.Count == 0)
                Console.WriteLine("No movies to show");
            foreach (var movie in movies)
            {
                Console.WriteLine("{0} ({1})", movie.Name, movie.YearOfRelease);
                Console.WriteLine("Plot - " + movie.Plot);
                Console.WriteLine();
                Console.Write("Actors - ");
                var actorNames = movie.Actors.Select(a => a.Name);
                Console.WriteLine(String.Join(", ", actorNames));
                Console.WriteLine("Producers - " + movie.Producer.Name);
                Console.WriteLine();
            }
        }
    }
}
