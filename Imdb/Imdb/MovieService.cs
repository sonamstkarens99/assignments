﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Imdb.Repository;
using Imdb.Domain;

namespace Imdb
{
    public class MovieService
    {
        private ActorRepository _actorRepository;
        private ProducerRepository _producerRepository;
        private MovieRepository _movieRepository;
        public MovieService()
        {
            _actorRepository = new ActorRepository();
            _producerRepository = new ProducerRepository();
            _movieRepository = new MovieRepository();
        }
        public void AddActor(string name, DateTime dateOfBirth)
        {
            if (String.IsNullOrEmpty(name))
            {
                throw new ArgumentException($"'{nameof(name)}' cannot be null or empty", nameof(name));
            }
            var hasActor = _actorRepository.GetAll().Where(a => a.Name.Equals(name) == 0).ToList();
            if (hasActor.Count != 0)
            {
                throw new Exception("Actor already added!");
            }

            _actorRepository.Add(
                new Actor
                {
                    Name = name,
                    DateOfBirth = dateOfBirth
                });
        }
        public void AddProducer(string name, DateTime dateOfBirth)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException($"'{nameof(name)}' cannot be null or empty", nameof(name));
            }
            var hasProducer = _producerRepository.GetAll().Where(a => a.Name.Equals(name) == 0).ToList();
            if (hasProducer.Count != 0)
            {
                throw new Exception("Producer already added!");
            }

            _producerRepository.Add(
                new Producer
                {
                    Name = name,
                    DateOfBirth = dateOfBirth
                });
        }

        public void AddMovie(string name, int yearOfRelease, string plot, List<Actor> actors, Producer producer)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException($"'{nameof(name)}' cannot be null or empty", nameof(name));
            }

            if (string.IsNullOrEmpty(plot))
            {
                throw new ArgumentException($"'{nameof(plot)}' cannot be null or empty", nameof(plot));
            }

            if (actors is null)
            {
                throw new ArgumentNullException("Choose at least 1 actor");
            }

            if (producer is null)
            {
                throw new ArgumentNullException("Choose exactly 1 producer");
            }
            var hasMovie = _movieRepository.GetAll().Where(a => a.Name.Equals(name) == 0).ToList();
            if (hasMovie.Count != 0)
            {
                throw new Exception("Movie already added!");
            }

            _movieRepository.Add(
                new Movie
                {
                    Name = name,
                    YearOfRelease = yearOfRelease,
                    Plot = plot,
                    Actors = actors,
                    Producer = producer
                });
        }
        public void DeleteMovie(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException($"'{nameof(name)}' cannot be null or empty", nameof(name));
            }

            var toDelete = _movieRepository.GetAll().Find(m => m.Name.Equals(name) == 0);
            if (toDelete == null)
            {
                throw new InvalidOperationException("cannot find that movie!");
            }
            _movieRepository.Delete(toDelete);
        }

        public void DeleteActor(string name)
        {
            var toDelete = _actorRepository.GetAll().Find(a => a.Name.Equals(name) == 0);
            if (toDelete == null)
            {
                throw new InvalidOperationException("cannot find the actor!");
            }
            _actorRepository.Delete(toDelete);
        }

        public List<Movie> GetAllMovies()
        {
            return _movieRepository.GetAll();
        }

        public List<Actor> ReturnActors()
        {
            return _actorRepository.GetAll();
        }
        public List<Producer> ReturnProducers()
        {
            return _producerRepository.GetAll();
        }
    }
}
