﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imdb.Domain
{
    public class Movie
    {
        public string Name { get; set; }
        public int YearOfRelease { get; set; }
        public string Plot { get; set; }
        public List<Actor> Actors { get; set; }
        public Producer Producer { get; set; }
    }
}
