﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Imdb.Domain;

namespace Imdb.Repository
{
    public class ProducerRepository
    {
        private List<Producer> _producer;
        public ProducerRepository()
        {
            _producer = new List<Producer>();
        }
        public void Add(Producer producer)
        {
            _producer.Add(producer);
        }
        public List<Producer> GetAll()
        {
            return _producer.ToList();
        }
    }
}
