﻿using System;
using System.Collections.Generic;
using System.Text;
using Imdb.Domain;
using System.Linq;

namespace Imdb.Repository
{
    public class MovieRepository
    {
        private List<Movie> _movie;
        public MovieRepository()
        {
            _movie = new List<Movie>();
        }
        public void Add(Movie movie)
        {
            _movie.Add(movie);
        }
        public void Delete(Movie movie)
        {
            _movie.Remove(movie);
        }
        public List<Movie> GetAll()
        {
            return _movie.ToList();
        }
    }
}
