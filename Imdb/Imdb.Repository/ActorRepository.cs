﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Imdb.Domain;

namespace Imdb.Repository
{
    public class ActorRepository
    {
        private List<Actor> _actor;
        public ActorRepository()
        {
            _actor = new List<Actor>();
        }
        public void Add(Actor actor)
        {
            _actor.Add(actor);
        }
        public void Delete(Actor actor)
        {
            _actor.Remove(actor);
        }
        public List<Actor> GetAll()
        {
            return _actor.ToList();
        }
    }
}
