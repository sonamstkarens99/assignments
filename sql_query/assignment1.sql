CREATE DATABASE Library;
USE Library

CREATE TABLE Actors(
		  Id INT PRIMARY KEY IDENTITY,
		  Name VARCHAR(100),
		  Gender VARCHAR(10),
		  DOB DATE
		);
		INSERT INTO Actors VALUES ('Mila Kunis','Female','11/14/1986')
		INSERT INTO Actors VALUES ('Robert DeNiro','Male','07/10/1957')
		INSERT INTO Actors VALUES ('George Michael','Male','11/23/1978')
		INSERT INTO Actors VALUES ('Mike Scott','Male','08/06/1969')
		INSERT INTO Actors VALUES ('Pam Halpert','Female','09/26/1996')
		INSERT INTO Actors VALUES ('Dame Judi Dench','Female','04/05/1947')

CREATE TABLE Producers(
		Id INT PRIMARY KEY IDENTITY,
		Name VARCHAR(100),
		Company VARCHAR(100),
		CompanyEstDate date
		);
		INSERT INTO Producers VALUES ('Arjun','Fox','05/14/1998')
		INSERT INTO Producers VALUES ('Arun','Bull','09/11/2014')
		INSERT INTO Producers VALUES ('Tom','Hanks','11/03/1987')
		INSERT INTO Producers VALUES ('Zeshan','Male','11/14/1996')
		INSERT INTO Producers VALUES ('Nicole','Team Coco','09/26/1992')
CREATE TABLE Movies(
	
	Id INT PRIMARY KEY IDENTITY,
    Name VARCHAR(100),
    Language VARCHAR(20),
    ProducerId INT FOREIGN KEY REFERENCES Producers(Id),
    Profit INT,
);
CREATE TABLE MovieActorMapping(
    MovieId INT
        FOREIGN KEY 
        REFERENCES 
        Movies(Id),
    ActorId INT 
        FOREIGN KEY 
        REFERENCES 
            Actors(Id)
);

SELECT * FROM MovieActorMapping
INSERT INTO MovieActorMapping
VALUES
    (1,1),
    (1,3),
    (1,5),
    (2,6),
    (2,5),
    (2,4),
    (2,2),
    (3,3),
    (3,2),
    (4,1),
    (4,6),
    (4,3),
    (5,2),
    (5,5),
    (5,3)

INSERT INTO Movies VALUES ('Rocky','English',1,'10000')
INSERT INTO Movies VALUES ('Rocky','Hindi',3,'3000')
INSERT INTO Movies VALUES ('Terminal','English',4,'300000')
INSERT INTO Movies VALUES ('Rambo','Hindi',2,'93000')
INSERT INTO Movies VALUES ('Rudy','English',5,'9600')


SELECT * FROM Movies
SELECT * FROM Producers
SELECT * FROM Actors

--Update Profit of all the movies by +1000 where producer name contains 'Arun'
UPDATE m
SET m.Profit = m.Profit + 1000
FROM Movies AS m
INNER JOIN Producers AS p
ON p.Id = m.ProducerId
WHERE p.Name = 'Arun'

--Find duplicate movies having the same name and their count


SELECT Movies.Name , COUNT(Movies.Name)
FROM Movies
GROUP BY Movies.Name
HAVING COUNT(Movies.Name)>1


--Find the oldest actor/actress for each movie

SELECT a.Name, c.Name, a.DOB FROM
(SELECT a.Name,  MAX(DOB) as DOB FROM Movies a
INNER JOIN MovieActorMapping b
ON a.Id = b.MovieId
INNER JOIN Actors c
ON c.Id = b.ActorId
GROUP BY a.Name) a
INNER JOIN Actors c
ON a.DOB = c.DOB




--List of producers who have not worked with actor X

SELECT P.Name
FROM Producers P
Where P.Id in(
			SELECT P.Id
            FROM Producers AS P
			INNER JOIN Movies m
			ON P.Id = m.ProducerId
			WHERE m.Id NOT IN( 
							SELECT b.MovieId FROM MovieActorMapping b
							INNER JOIN Actors a
							ON a.Id = b.ActorId
							WHERE a.Name = 'George Michael'
							)
            )
--List of pair of actors who have worked together in more than 2 movies

SELECT Actor_1_Name, Actor_2_Name, COUNT(MovieId) AS Movie_Count
FROM (	

		SELECT A1.Name AS Actor_1_Name, A2.Name AS Actor_2_Name, MAM2.MovieId
		FROM MovieActorMapping AS MAM1
		JOIN MovieActorMapping AS MAM2
		ON MAM1.MovieId = MAM2.MovieId
		AND MAM1.ActorId < MAM2.ActorId
		JOIN Actors AS A1
		ON A1.Id = MAM1.ActorId
		JOIN Actors AS A2
		ON A2.Id = MAM2.ActorId) AS P
		GROUP BY Actor_1_Name, Actor_2_Name
		HAVING COUNT(MovieId) > 1

--Add non-clustered index on profit column of movies table

CREATE NONCLUSTERED INDEX ix_producers_profit
ON Movies(Profit DESC)

--Create stored procedure to return list of actors for given movie id

CREATE PROCEDURE ListActors
AS 
SELECT a.Name, b.MovieId FROM Actors a
INNER JOIN MovieActorMapping b
ON a.Id = b.ActorId
INNER JOIN Movies m
ON m.Id = b.MovieId
GO
EXEC ListActors;

--Create a function to return age for given date of birth

DROP FUNCTION IF EXISTS AGE

CREATE FUNCTION AGE(@DateOfBirth DATE)
RETURNS INT
AS
BEGIN
		RETURN DATEDIFF(YY,@DateOfBirth,GETDATE())
END
	SELECT dbo.AGE(dob)  FROM Actors

-
