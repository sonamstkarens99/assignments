CREATE DATABASE SchoolName
USE SchoolName
CREATE TABLE Students(
						StudentId INT PRIMARY KEY IDENTITY, 
						StudentName VARCHAR(100), 
						DOB date, 
						GENDER VARCHAR (9),
						ClassId INT FOREIGN KEY REFERENCES Classes(ClassId)
						);

INSERT INTO Students values ('Scotty Loman','2006/01/31','Male',1)
INSERT INTO Students values ('Adam Scott','2005/06/01','Male',1)
INSERT INTO Students values ('Natosha Beckles','2005/01/23','Female',2)
INSERT INTO Students values ('Lilly Page','2006/11/26','Female',2)
INSERT INTO Students values ('John Freeman','2006/06/14','Male',2)
INSERT INTO Students values ('Morgan Scott','2005/05/18','Male',3)
INSERT INTO Students values ('Codi Gass','2005/12/24','Female',3)
INSERT INTO Students values ('Nick Roll','2005/12/24','Male',3)
INSERT INTO Students values ('Dave Grohl','2005/02/12','Male',3)

CREATE TABLE Classes (
					ClassId INT PRIMARY KEY IDENTITY, 
					Name VARCHAR(10) , 
					Section VARCHAR(10),
					Number INT 
					);
INSERT INTO Classes values (1,'IX','A',201)
INSERT INTO Classes values (2,'IX','B',202)
INSERT INTO Classes values (3,'X','A',203)

CREATE TABLE Teacher(
					TeacherId INT PRIMARY KEY IDENTITY ,  
					Name VARCHAR(100), 
					DOB date, 
					Gender VARCHAR (9)
					);
INSERT INTO Teacher values (1,'Lisa Kudrow','1985/06/08','Female')
INSERT INTO Teacher values (2,'Monica Bing','1982/03/06','Female')
INSERT INTO Teacher values (3,'Chandler Bing','1978/12/17','Male')
INSERT INTO Teacher values (4,'Ross Geller','1993/01/26','Male')
CREATE TABLE TeacherClassMapping (
			TeacherId INT FOREIGN KEY REFERENCES Teacher(TeacherId),
			ClassId INT FOREIGN KEY REFERENCES Classes(ClassId)		
			);
INSERT INTO TeacherClassMapping values (1,1)
INSERT INTO TeacherClassMapping values (1,2)
INSERT INTO TeacherClassMapping values (2,2)
INSERT INTO TeacherClassMapping values (2,3)
INSERT INTO TeacherClassMapping values (3,3)
INSERT INTO TeacherClassMapping values (3,1)

SELECT * FROM TeacherClassMapping
SELECT * FROM Students
SELECT * FROM Classes



--Find list of male students 
SELECT *  FROM Students WHERE Gender = 'Male';

--Find list of student older than 2005/01/01
SELECT *  FROM Students WHERE DOB > '2005/01/01';

--Youngest student in school
SELECT * FROM Students where dob = (SELECT MIN(DOB) FROM Students)

--Find student distinct birthdays
SELECT DISTINCT DOB FROM Students;

-- No of students in each class
SELECT C.Name, Count(S.StudentId)
FROM Students S
INNER JOIN Classes C
ON C.ClassID = S.ClassId
GROUP BY C.Name

-- No of students in each section
SELECT C.Section,Count(S.StudentId)
FROM Students S
INNER JOIN Classes C
ON C.ClassID = S.ClassId
GROUP BY C.Section

-- No of classes taught by teacher
SELECT T.TeacherId, COUNT(C.ClassId)
FROM TeacherClassMapping T
INNER JOIN Classes C
ON T.TeacherID = C.ClassId
GROUP BY T.TeacherId

-- List of teachers teaching Class X
SELECT Teacher.TeacherId,Teacher.Name
FROM Teacher INNER JOIN TeacherClassMapping 
ON Teacher.TeacherId = TeacherClassMapping.TeacherId
INNER JOIN Classes 
ON Classes.ClassId = TeacherClassMapping.TeacherId
WHERE CLASSES.Name = 'X'


-- Classes which have more than 2 teachers teaching
SELECT  Classes.ClassId
FROM Teacher INNER JOIN TeacherClassMapping 
ON Teacher.TeacherId = TeacherClassMapping.TeacherId 
INNER JOIN Classes
ON Classes.ClassId = TeacherClassMapping.ClassId
GROUP BY Classes.ClassId,Classes.Name
HAVING COUNT(TeacherClassMapping.TeacherId) > 2

-- List of students being taught by 'Lisa'
SELECT Students.StudentName
FROM Teacher INNER JOIN TeacherClassMapping 
ON Teacher.TeacherId = TeacherClassMapping.TeacherId 
INNER JOIN Classes
ON Classes.ClassId = TeacherClassMapping.ClassId
INNER JOIN Students
ON Classes.ClassId = Students.ClassId  
WHERE Teacher.Name = 'Lisa Kudrow'



