function edit(id){
    $.ajax({
        type:'GET',
        dataType:'json',
        url:'https://localhost:44386/api/Movie/' + id,
        contentType: "application/json",
        success: function (data) {
            localStorage.setItem('list', JSON.stringify(data));
        window.open("index.html");
            
        }
    });
}
var finalValues;
$(document).ready(function(){ 
    var id1 = localStorage.getItem('list');
    finalValues = JSON.parse(id1);
     console.log(id1);
    //  console.log(finalValues);
    $("#movie-name").val(finalValues.name);
    $("#plot").val(finalValues.plot);
     $("#yor").val(finalValues.yor.split("T")[0]);
     $("#producers").val(finalValues.producer);
    //  $("#producers").val(finalValues.producer.name);
  // Create the DOM option that is pre-selected by default
  var producerOption = new Option(finalValues.producer.name, finalValues.producer.id, true, true);
  $("#producers").append(producerOption);
  for(var i=0; i<finalValues.actor.length;i++){
         var actorOption = new Option(finalValues.actor[i].name, finalValues.actor[i].id, true, true);
         $("#actors").append(actorOption);
    
  }
  for(var j=0;j<finalValues.genre.length;j++){
      var genreOption = new Option(finalValues.genre[j].name, finalValues.genre[j].id, true , true);
      $("#genres").append(genreOption);
  }
  localStorage.removeItem("list"); 
});
function update(){
var movie=$("#movie-name").val();
var yo = $("#yor").val();
var plt = $("#plot").val();

var act = $("#actors").val();
var actorId = act.map((i) => Number(i));
var producer =parseInt($("#producers").val());
var genre = $("#genres").val();
var genreId = genre.map((i) => Number(i));
console.log(movie);
var movieList ={
    name:movie,
    yor:yo,
    plot:plt,
    actor:actorId,
    producerId:producer,
    genre:genreId
};
console.log(movieList);
$.ajax({
    type:'PUT',
    url: 'https://localhost:44386/api/Movie/' + finalValues.id,
    contentType: "application/json",
    data: JSON.stringify(movieList),
    success: function () {
     window.location.href="moviehome.html";
   
    }
});



}
