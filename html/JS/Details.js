$(function(){
    getActorList()
    getGenreList()
    getProducerList()

})
function getGenreList(){
    var genre = $("#genres");
    $.ajax({
        type:'GET',
        datatype:'json',
        url:'https://localhost:44386/api/Genre',
        success:function(data){
            $.each(data,function(i,data){
                // genre.append("<option value='" + data.id + "'>" + data.name+"</option>");
                genre.append("<option value='" +data.id + "'>" + data.name+"</option>");
                console.log(data.id);
            })
        }
    })
};
function getProducerList(){
    var producer = $("#producers");
    $.ajax({
        type:'GET',
        datatype:'json',
        url:'https://localhost:44386/api/Producer',
        success:function(data){
            $.each(data,function(i,data){
                producer.append("<option value='" + data.id + "'>" + data.name+"</option>");
                console.log(data.id);
            })
        }
    })
//    document.getElementById("actorForm").reset();
};
function getActorList(){
    var actor = $("#actors");
    console.log(actor);
    $.ajax({
        type:'GET',
        datatype:'json',
        url:'https://localhost:44386/api/Actor',
        success:function(data){
            $.each(data, function(i,data){
                actor.append("<option value='" + data.id + "'>" + data.name + "</option>");
                // console.log("<option value='" + data.id + "'>" + data.name + "</option>");
                
            })
           
        }
    });
};