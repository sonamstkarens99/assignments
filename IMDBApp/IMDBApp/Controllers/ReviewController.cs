﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBApp.Controllers
{
    [Route("api/Review")]
    [ApiController]
    public class ReviewController : ControllerBase
    {
        //GET: api/ReviewController
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("get");
        }
        //GET: api/ReviewController/1
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Ok("get id");
        }
    
    }
}
